/*Author: Matthew Leong
  Description: This is a discord bot that can be used to censor profane words from
  the chat.  It also has added functionality to play music, specifically from
  Youtube, into a discord voice chat.  Note this bot requires ffmpeg to use music
  playing functionality.
  Some resources I used:
  https://www.youtube.com/watch?v=-5jJaO17Gjs. Aeirety's guide on discord bots.
  https://discord.js.org/#/docs/main/stable/general/welcome. Discord.js API. */


//const you can't change discord variable
//discord library is inside discord
//client is inside discord and will connect to the discord server
const Discord = require('discord.js');
const YTDL = require("ytdl-core");
//bot token is a password of sorts to the bot if anyone knows the token they
//can just paste it in like i just did here
//you can generate a new token in that case
//const commando = require('discord.js-commando')
//commando is a discord client with extra features
const bot = new Discord.Client();

//making a test prefix for expletives. later maybe have an array of these words
/*I think I'm using prefixes wrong here. The prefix is probably used as a command
word and then anything follows is the specific command in that family.  For
instance kick could be a prefix word and then the next word could be the specific
user.*/
const PREFIX = "cmd";

//This is the pattern to match against music links to check if they are youtube
//videos.
var patt = 'https://www.youtube.com/watch';

//play function
function play(connection, message){
  var server = servers[message.guild.id];
  server.dispatcher = connection.playStream(YTDL(server.queue[0], {filter: 'audioonly'}));

  server.queue.shift();
  server.dispatcher.on('end', function() {
      if(server.queue[0]){
        play(connection, message);
      } else connection.disconnect();
  });
}


var filterOn = true;
var kickWords = ['fuck', 'shit', 'bitch']
//queue for music over different servers. not super necessary because I'm using
//the bot in one server.  necessary for public bots.
var servers = {};

//function for when the bot logs in. introduction



//this function logs every message in the chat to my console
/*bot.on("message", function(message) {
    //this line will exclude messages from the bot in the log
    if(message.author.equals(bot.user)) return;
    console.log(message.content);
});*/

//every command has to be registered in a group
bot.on('message', (message) => {
//Checks for they keyword mert and politely reminds the chat that he is to be
//referred to as the golden god.
    if(message.content.toLocaleLowerCase().includes('mert')){
        message.channel.sendMessage(message.content.replace(/mert/i, '*Golden God*'));
    }

/*This is the profanity filter that matches kickable words against any message
in the channel. So far the bot can only respond with a bad language message. */
    if(filterOn){
        var bool = true;
          for(var i = 0; i < kickWords.length && bool; i++){
              if(message.content.toLocaleLowerCase().includes(kickWords[i])){
                  message.delete();
                  message.channel.sendMessage('Bad language from ' +
                  message.author.username);
                  //if i use an explicative the bot will actually correct my name
                  //to golden god because it also checks its own message rn. not
                  //sure if this a bug or a feature.
                    bool = false;
                  }
                }
              }
    //command list for the bot. must be preceded by cmd keyword
    if(message.content.startsWith(PREFIX)){
      //PREFIX.length + 1 to account for the space between cmd and the keywords
      var args = message.content.substring(PREFIX.length + 1).split(" ");

      //args is necessary for commands with multiple parts like play and link
      //also might want to wrap these commands with a check to make sure I'm
      //making the commands
      switch (args[0].toLocaleLowerCase()){
          //can't use uppercases in the case names because the switch converts
          //everything to lower case
          case 'help':
              message.channel.sendMessage('Here is a list of cmd commands:\n' +
              'cmd filteroff = turns off profanity filter.\n' +
              'cmd filteron = turns on profanity filter.\n');
              break;
          case 'filteroff':
              filterOn = false;
              message.channel.sendMessage('Profanity filter off.');
              break;
          case 'filteron':
              filterOn = true;
              message.channel.sendMessage('Profanity filter on.');
              break;
          case 'play':
              if(!args[1]){
                  message.channel.sendMessage('No link provided.');
                  return;
              }
              if(!args[1].includes(patt)){
                  message.channel.sendMessage('Link is not a youtube video.');
                  return;
              }
              if(!message.member.voiceChannel){
                  message.channel.sendMessage('You must be in the voice channel to request music.');
                  return;
              }
              //TO DO link check goes around here. http://www.youtube.com/watch
              if(!servers[message.guild.id]){
                  servers[message.guild.id] = {queue: []}
              }

              var server = servers[message.guild.id];

              server.queue.push(args[1]);
              //if not in the voice channel enter
              if(!message.guild.voiceConnection){
                  message.member.voiceChannel.join().then(function(connection){
                        play(connection, message);
                  });
              }
              break;
          case 'skip':
              var server = servers[message.guild.id];
              //if there is a dispatcher end that dispatcher
              if(server.dispatcher){
                server.dispatcher.end();
              }
              break;
          case 'stop':
              var server = servers[message.guild.id];

              if(message.guild.voiceConnection){
                message.guild.voiceConnection.disconnect();
              }
              break;
          default:
              message.channel.sendMessage('Invalid command.')
      }
    }
});

bot.login('Mzk4MjI1NjkwNTk3NTIzNDU2.DT5obw.rTJmr5UWAIOyljsXUbMLZlV-AUY');
//bot id goes in the parathesis above
//to restart bot end node js serverside javascript in task manager
//bot is always live for a few more minutes will go offline due to inactivity
